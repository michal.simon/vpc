locals {
  count = length(var.availability_zones)
}

resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr

  tags = {
      Name = upper("VPC_${var.name}")
  }
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id
    tags = {
      Name = upper("IGW_${var.name}")
  }
}

resource "aws_subnet" "public" {
  vpc_id = aws_vpc.this.id
  map_public_ip_on_launch = true
  count = local.count
  cidr_block = "${var.subnet_prefix}.${var.start_ip+count.index}.0/24"
  availability_zone = var.availability_zones[count.index]
  
  tags = {
    Name = "Public-${var.name}-${var.availability_zones[count.index]}"
    Tier = "Public"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = {
      Name = upper("RT_PUBLIC_${var.name}")
  }
}

resource "aws_route_table_association" "this" {
  count = local.count
  subnet_id = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}

resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.this]
}

resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = element(aws_subnet.public.*.id, 0)
  depends_on    = [aws_internet_gateway.this]
  tags = {
    Name        = "${var.name}-nat-gateway"
  }
}

resource "aws_default_route_table" "this" {
  default_route_table_id = aws_vpc.this.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.this.id
  }

  tags = {
    Name = upper("RT_PRIVATE_${var.name}")
  }
}