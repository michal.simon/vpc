variable "name" {
  description = "Application Name"
  type = string
}

variable "vpc_cidr" {
  description = "CIDR of the VPC"
  type = string
}

variable "subnet_prefix" {
  description = "Prefix for setup of subnets"
  type = string
}

variable "availability_zones" {
  description = "AWS AVailability Zones"
  type = list
}

variable "start_ip" {
  description = "Start ip of public subnets"
  type = string
}