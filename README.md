# Terraform VPC Module

[![Codeac.io](https://static.codeac.io/badges/3-15882460.svg)](https://app.codeac.io/gitlab/terraform147/vpc)

## Contains

- VPC
- Public Subnets
- Public Route Table
- Internet Gateway
- Elastic Ip for Nat Gateway
- Nat Gateway

## Outputs

- VPC Id
- Public Subnets Id's

## How to use

### Setup Module
```
module "vpc" {
  source = "git@gitlab.com:terraform147/vpc.git"
  name = ""
  vpc_cidr = ""
  subnet_prefix = ""
  start_ip = ""
  availability_zones = ""
}
```

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: